@extends('templates/main')

@section('css')
<style>

</style>
@endsection

@section('content')
<div class="container justify text-center">
    <div class="col-lg-12">
        <div class="d-flex justify-content-between">
            <div class="container">

                <div class="row align-items-center form-group">

                    <div class="col-md-4 text-left">
                        @can('superadmin_pabrik')
                        <h4>Retur Pabrik ke Supplier</h4>
                        @endcan
                        @can('superadmin_distributor')
                        <h4>Retur Distributor ke Pabrik</h4>
                        @endcan
                        @can('reseller')
                        <h4>Retur Reseller ke Distributor</h4>
                        @endcan
                    </div>
                    <div class="col-md-5">
                        <button type="button" class="btn btn-primary" onclick="window.location.href='{{ url('/retur/to_supplier/export/') }}'">
                            <i class="fa fa-file-pdf-o"></i>
                            <span>Export</span>
                        </button>
                        <button type="button" class="btn btn-primary" onclick="window.open('{{ url('/retur/to_supplier/print/') }}')">
                            <i class="fa fa-print"></i>
                            <span>Print</span>
                        </button>
                    </div>
                    <div class="col-md-3 text-right">
                        <button type="button" class="btn btn-primary" onclick="location.href='{{ url('/retur/to_supplier/create/') }}'">
                            <span>+ Add</span>
                        </button>
                    </div>

                </div>
                
            </div>
        </div>
    </div>

    <hr>

    <div class="row w-100">
        <div class="col-12 grid-margin">
            <div class="iq-card">
                <div class="iq-card-body">
                    <table id="myTable" class="table table-hover table-striped table-light table-responsive text-left text-nowrap" >
                        <thead>
                            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                                @if(auth()->user()->user_position == "sales")
                                <th scope="col">Tanggal Terima Barang</th>
                                @else
                                <th scope="col">No Nota</th>
                                @endif
                                <th scope="col">Supplier</th>
                                <th scope="col">No Surat Keluar</th>
                                <th scope="col">Tanggal Retur</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($returs as $retur)
                                <tr>
                                    @if(auth()->user()->user_position == "sales")
                                    <td scope="col">{{ $retur->sales_stok->updated_at->format('d/m/y H:i:s') }}</td>
                                    @else
                                    <td scope="col">{{ $retur->transaction->transaction_code }}</td>
                                    @endif
                                    <td scope="col">
                                        {{ $retur->supplier->firstname }} {{ $retur->supplier->lastname }} 
                                    </td>
                                    <td scope="col">{{ $retur->surat_keluar }}</td>
                                    <td scope="col">{{ $retur->updated_at->format('d/m/y H:i:s') }}</td>
                                    <td scope="col">{{ $retur->keterangan }}</td>
                                    <td scope="col">
                                        @if($retur->status_retur == 0)
                                            Menunggu Konfirmasi
                                        @else
                                            Retur Sukses
                                        @endif
                                    </td>
                                    <td>
                                        <div class="form-group" >
                                            <button type="button" onclick="location.href='{{ url('/retur/to_supplier/detail/'.$retur->id) }}'" class="btn btn-primary">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
@if ($message = Session::get('create_retur_success'))
    swal(
        "Berhasil!",
        "{{ $message }}",
        "success"
    );
@endif

$(document).ready(function(){
    $('#myTable').DataTable(
        {
        "oSearch": { "bSmart": false, "bRegex": true },
        }
    );
});

</script>
@endsection