<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Barang</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">
        Daftar Distributor - {{ date('F Y') }}
    </p>
    <table class="table table-hover table-striped table-light display sortable  text-nowrap" cellspacing="0"  id="myTable" >
        <thead>
            <tr>
                <th >ID</th>
                <th >Distributor</th>
            </tr>
        </thead>

        <tbody>
            @foreach($distributors as $distributor)
                <tr>
                    <td>{{ $distributor->id }}</td>
                    <td>{{ $distributor->firstname }} {{ $distributor->lastname }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p style="font-weight:bold">Total Distributor : {{ $distributors->count() }} Distributor</p>
</body>
</html>