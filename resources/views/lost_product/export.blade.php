<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Account</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">Laporan Barang Hilang</p>
    <table class="table table-hover table-light" id="myTable">
        <thead>
            <tr>
                <th scope="col">Kode Barang</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Sisa Barang</th>
                <th scope="col">Stok Real</th>
                <th scope="col">Barang Hilang</th>
                <th scope="col">Kerugian</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lost_products as $lost_product)
                <tr>
                    <td>{{ $lost_product->product->product_type->kode_produk }}</td>
                    <td>{{ $lost_product->nama_produk }}</td>
                    <td>{{ $lost_product->stok_sisa }} pcs</td>
                    <td>{{ $lost_product->stok_real }} pcs</td>
                    <td>{{ $lost_product->stok_hilang }} pcs</td>
                    <td>Rp. {{ $lost_product->total_kerugian }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>