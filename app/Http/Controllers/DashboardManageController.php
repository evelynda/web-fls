<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductType;
use App\Models\ReturDetail;
use App\Models\ReturHistory;
use App\Models\SalesStokDetail;
use App\Models\SalesStokHistory;
use App\Models\SupplyDetail;
use App\Models\SupplyHistory;
use App\Models\TransactionHistory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use App\Models\TrackingSalesDetail;
use App\Models\TrackingSalesHistory;
use PDF;
use App\Models\TransactionDetailSell;
use App\Models\TransactionHistorySell;

class DashboardManageController extends Controller
{
    public function viewDashboard()
    {
        $MAXSHOW = 3;
        if(auth()->user()->id_group == 1)
        {
            // pusat
            $pusat = User::where('id_group', auth()->user()->id_group)->where('user_position', 'superadmin_pabrik')->first();

            $stokPenjualan = TransactionDetail::join('transaction_histories', 'transaction_histories.id', '=', 'transaction_details.id_transaction')->where('transaction_histories.id_distributor', $pusat->id)->where('status_pesanan', 1)->sum('jumlah');
            $totalPenjualan = TransactionHistory::where('id_distributor', $pusat->id)->where('status_pesanan', 1)->sum('total');

            $stokPembelian = SupplyDetail::join('supply_histories', 'supply_histories.id', '=', 'supply_details.id_supply')->sum('jumlah');
            $totalPembelian = SupplyHistory::sum('total');

            $products = Product::where('id_owner', $pusat->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*', 'product_types.*')->get();
            $stok = $products->sum('stok');

            // distributors
            $distributors = User::where('user_position', 'superadmin_distributor')->get();

            $distributorStoks = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->take($MAXSHOW)->get();
            
            $distributors->map(function ($dist) {
                $totalPenjualan1 = TransactionHistory::where('id_distributor', $dist['id'])->sum('total');
                $totalPenjualan2 = TransactionHistorySell::where('id_owner', $dist['id'])->sum('total');

                $dist['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                return $dist;
            });
            $distributorPenjualans = $distributors->sortByDesc(function($d)
            {
                return $d->totalPenjualan;
            });
            $distributorPenjualans = $distributorPenjualans->take($MAXSHOW);

            $distributorReturs = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'superadmin_distributor')->where('status_retur', 1)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->take($MAXSHOW)->get();

            // resellers + sales
            $resellers = User::where('user_position', 'reseller')->orWhere('user_position', 'sales')->orderBy('id_group', 'asc')->get();
            $resellerStoks = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->take($MAXSHOW)->get();

            $resellers->map(function ($res) {
                $totalPenjualan1 = TrackingSalesHistory::where('id_reseller', $res['id'])->sum('total');
                $totalPenjualan2 = TransactionHistorySell::where('id_owner', $res['id'])->sum('total');

                $res['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                return $res;
            });
            $resellerPenjualans = $resellers->sortByDesc(function($d)
            {
                return $d->totalPenjualan;
            });
            $resellerPenjualans = $resellerPenjualans->take($MAXSHOW);

            $resellerReturs = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->where('status_retur', 1)->orWhere('user_position', 'sales')->where('status_retur', 1)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->take($MAXSHOW)->get();

            return view('dashboard.index', compact(['stokPenjualan', 'totalPenjualan', 'stokPembelian', 'totalPembelian', 'products', 'stok', 'distributorStoks', 'distributorPenjualans', 'distributorReturs', 'resellerStoks', 'resellerPenjualans', 'resellerReturs']));
        }
        else if(auth()->user()->id_group != 1 && auth()->user()->user_position != "reseller" && auth()->user()->user_position != "sales")
        {
            // distributor
            $distributor = User::where('id_group', auth()->user()->id_group)->where('user_position', 'superadmin_distributor')->first();

            $stokPenjualan1 = TransactionDetail::join('transaction_histories', 'transaction_histories.id', '=', 'transaction_details.id_transaction')->where('transaction_histories.id_distributor', $distributor->id)->where('status_pesanan', 1)->sum('jumlah');
            $stokPenjualan2 = TransactionDetailSell::join('transaction_history_sells', 'transaction_history_sells.id', '=', 'transaction_detail_sells.id_transaction')->where('transaction_history_sells.id_owner', $distributor->id)->sum('jumlah');
            $stokPenjualan = $stokPenjualan1 + $stokPenjualan2;
            
            $totalPenjualan1 = TransactionHistory::where('id_distributor', $distributor->id)->where('status_pesanan', 1)->sum('total');
            $totalPenjualan2 = TransactionHistorySell::where('id_owner', $distributor->id)->sum('total');
            $totalPenjualan = $totalPenjualan1 + $totalPenjualan2;

            $stokPembelian = TransactionDetail::join('transaction_histories', 'transaction_histories.id', '=', 'transaction_details.id_transaction')->where('transaction_histories.id_owner', $distributor->id)->where('status_pesanan', 1)->sum('jumlah');
            $totalPembelian = TransactionHistory::where('id_owner', $distributor->id)->where('status_pesanan', 1)->sum('total');

            $stokRetur = ReturDetail::join('retur_histories', 'retur_histories.id', '=', 'retur_details.id_retur')->where('retur_histories.id_owner', $distributor->id)->where('status_retur', 1)->sum('jumlah');
            $totalRetur = ReturHistory::where('id_owner', $distributor->id)->where('status_retur', 1)->sum('total');
            
            $products = Product::where('id_owner', $distributor->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*', 'product_types.*')->get();
            $stok = $products->sum('stok');
            
            // resellers + sales
            $resellerStoks = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->where('users.id_group', $distributor->id_group)->orWhere('user_position', 'sales')->where('users.id_group', $distributor->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->take($MAXSHOW)->get();
            
            $resellers = User::where('user_position', 'reseller')->orWhere('user_position', 'sales')->where('users.id_group', $distributor->id_group)->where('users.id_group', $distributor->id_group)->get();
            $resellers->map(function ($res) {
                $totalPenjualan1 = TrackingSalesHistory::where('id_reseller', $res['id'])->sum('total');
                $totalPenjualan2 = TransactionHistorySell::where('id_owner', $res['id'])->sum('total');

                $res['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                return $res;
            });
            $resellerPenjualans = $resellers->sortByDesc(function($d)
            {
                return $d->totalPenjualan;
            });
            $resellerPenjualans = $resellerPenjualans->take($MAXSHOW);

            // $resellerReturs = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->where('users.id_group', $distributor->id_group)->orWhere('user_position', 'sales')->where('users.id_group', $distributor->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->take($MAXSHOW)->get();
            $resellerReturs = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('users.user_position', 'reseller')->where('users.id_group', $distributor->id_group)->where('retur_histories.status_retur', 1)->orWhere('users.user_position', 'sales')->where('users.id_group', $distributor->id_group)->where('retur_histories.status_retur', 1)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->take($MAXSHOW)->get();
            // $resellerReturs = User::crossJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('users.user_position', 'reseller')->where('users.id_group', $distributor->id_group)->orWhere('users.user_position', 'sales')->where('users.id_group', $distributor->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->take($MAXSHOW)->get();


            // dd($resellerReturs);
            return view('dashboard.index', compact(['distributor', 'stokPenjualan', 'totalPenjualan', 'stokPembelian', 'totalPembelian', 'stokRetur', 'totalRetur', 'products', 'stok', 'resellerStoks', 'resellerPenjualans', 'resellerReturs', 'resellers']));
        }
        else if(auth()->user()->user_position == "reseller")
        {
            $reseller = auth()->user();

            $stokPenjualan = TransactionDetailSell::join('transaction_history_sells', 'transaction_history_sells.id', '=', 'transaction_detail_sells.id_transaction')->where('transaction_history_sells.id_owner', $reseller->id)->sum('jumlah');
            $totalPenjualan = TransactionHistorySell::where('id_owner', $reseller->id)->sum('total');

            $stokPembelian = TransactionDetail::join('transaction_histories', 'transaction_histories.id', '=', 'transaction_details.id_transaction')->where('transaction_histories.id_owner', $reseller->id)->where('status_pesanan', 1)->sum('jumlah');
            $totalPembelian = TransactionHistory::where('id_owner', $reseller->id)->where('status_pesanan', 1)->sum('total');

            $stokRetur = ReturDetail::join('retur_histories', 'retur_histories.id', '=', 'retur_details.id_retur')->where('retur_histories.id_owner', $reseller->id)->where('status_retur', 1)->sum('jumlah');
            $totalRetur = ReturHistory::where('id_owner', $reseller->id)->where('status_retur', 1)->sum('total');

            $products = Product::where('id_owner', $reseller->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*', 'product_types.*')->get();
            $stok = $products->sum('stok');

            return view('dashboard.index', compact(['reseller', 'stokPenjualan', 'totalPenjualan', 'stokPembelian', 'totalPembelian', 'stokRetur', 'totalRetur', 'products', 'stok']));
        }
        else if(auth()->user()->user_position == "sales")
        {
            $sales = auth()->user();

            $stokPenjualan = TrackingSalesDetail::join('tracking_sales_histories', 'tracking_sales_histories.id', '=', 'tracking_sales_details.id_tracking_sales')->where('tracking_sales_histories.id_reseller', $sales->id)->sum('jumlah');
            $totalPenjualan = TrackingSalesHistory::where('id_reseller', $sales->id)->sum('total');

            $stokPengambilan = SalesStokDetail::join('sales_stok_histories', 'sales_stok_histories.id', '=', 'sales_stok_details.id_sales_stok')->where('sales_stok_histories.id_owner', $sales->id)->where('status', 1)->sum('jumlah');
            $totalPengambilan = SalesStokHistory::where('id_owner', $sales->id)->where('status', 1)->sum('total');

            $stokRetur = ReturDetail::join('retur_histories', 'retur_histories.id', '=', 'retur_details.id_retur')->where('retur_histories.id_owner', $sales->id)->where('status_retur', 1)->sum('jumlah');
            $totalRetur = ReturHistory::where('id_owner', $sales->id)->where('status_retur', 1)->sum('total');

            $products = Product::where('id_owner', $sales->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*', 'product_types.*')->get();
            $stok = $products->sum('stok');

            return view('dashboard.index', compact(['sales', 'stokPenjualan', 'totalPenjualan', 'stokPengambilan', 'totalPengambilan', 'stokRetur', 'totalRetur', 'products', 'stok']));
        }
    }

    public function print()
    {
        if(auth()->user()->id_group == 1)
        {
            $owner = User::where('id', 1)->first();
        }
        else if(auth()->user()->user_position != "reseller" && auth()->user()->user_position != "sales")
        {
            $owner = User::where('id_group', auth()->user()->id_group)->where('user_position', 'superadmin_distributor')->first();
        }
        else if(auth()->user()->user_position == "reseller" || auth()->user()->user_position == "sales")
        {
            $owner = auth()->user();
        }

        $products = Product::where('id_owner', $owner->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*')->get();
        $types = ProductType::all();
        $stock = Product::where('id_owner', $owner->id)->sum('stok');

        return view('dashboard.print', compact(['owner', 'products', 'types', 'stock']));
    }

    public function export()
    {
        if(auth()->user()->id_group == 1)
        {
            $owner = User::where('id', 1)->first();
        }
        else if(auth()->user()->user_position != "reseller" && auth()->user()->user_position != "sales")
        {
            $owner = User::where('id_group', auth()->user()->id_group)->where('user_position', 'superadmin_distributor')->first();
        }
        else if(auth()->user()->user_position == "reseller" || auth()->user()->user_position == "sales")
        {
            $owner = auth()->user();
        }

        $products = Product::where('id_owner', $owner->id)->join('product_types', 'product_types.id', '=', 'products.id_productType')->orderBy('product_types.kode_produk', 'asc')->select('products.*')->get();
        $types = ProductType::all();
        $stock = Product::where('id_owner', $owner->id)->sum('stok');

        $pdf = PDF::loadView('dashboard.export', compact(['owner', 'products', 'types', 'stock']));

        if(auth()->user()->id_group == 1)
        {
            return $pdf->download('Stock Barang Pusat - ' .date('F Y').'.pdf');
        }
        else
        {
            return $pdf->download('Stock Barang '.$owner->firstname.' '.$owner->lastname.' - '.date('F Y').'.pdf');
        }
      
    }

    public function viewDetailTable($user, $type)
    {
        if($user == "distributor")
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
            }
            else if($type == "penjualan")
            {
                $distributors = User::where('user_position', 'superadmin_distributor')->get();
                $distributors->map(function ($dist) {
                    $totalPenjualan1 = TransactionHistory::where('id_distributor', $dist['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $dist['id'])->sum('total');
    
                    $dist['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $dist;
                });
                $datas = $distributors->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
            }

            return view('dashboard.detail_table', compact(['user', 'type', 'datas']));
        }
        else
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                if(auth()->user()->id_group == 1)
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
                else
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
            }
            else if($type == "penjualan")
            {
                if(auth()->user()->id_group == 1)
                {
                    $resellers = User::where('user_position', 'reseller')->orWhere('user_position', 'sales')->orderBy('id_group', 'asc')->get();
                }
                else
                {
                    $resellers = User::where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->orderBy('id_group', 'asc')->get();
                }

                $resellers->map(function ($res) {
                    $totalPenjualan1 = TrackingSalesHistory::where('id_reseller', $res['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $res['id'])->sum('total');
    
                    $res['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $res;
                });
                $datas = $resellers->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                if(auth()->user()->id_group == 1)
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
                else
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
            }

            return view('dashboard.detail_table', compact(['user', 'type', 'datas']));
        }

        return back();
    }

    public function print_detail_table($user, $type)
    {
        if($user == "distributor")
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
            }
            else if($type == "penjualan")
            {
                $distributors = User::where('user_position', 'superadmin_distributor')->get();
                $distributors->map(function ($dist) {
                    $totalPenjualan1 = TransactionHistory::where('id_distributor', $dist['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $dist['id'])->sum('total');
    
                    $dist['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $dist;
                });
                $datas = $distributors->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
            }

            return view('dashboard.print_detail_table', compact(['user', 'type', 'datas']));
        }
        else
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                if(auth()->user()->id_group == 1)
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
                else
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
            }
            else if($type == "penjualan")
            {
                if(auth()->user()->id_group == 1)
                {
                    $resellers = User::where('user_position', 'reseller')->orWhere('user_position', 'sales')->orderBy('id_group', 'asc')->get();
                }
                else
                {
                    $resellers = User::where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->orderBy('id_group', 'asc')->get();
                }

                $resellers->map(function ($res) {
                    $totalPenjualan1 = TrackingSalesHistory::where('id_reseller', $res['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $res['id'])->sum('total');
    
                    $res['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $res;
                });
                $datas = $resellers->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                if(auth()->user()->id_group == 1)
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
                else
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
            }

            return view('dashboard.print_detail_table', compact(['user', 'type', 'datas']));
        }
    }

    public function export_detail_table($user, $type)
    {
        if($user == "distributor")
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
            }
            else if($type == "penjualan")
            {
                $distributors = User::where('user_position', 'superadmin_distributor')->get();
                $distributors->map(function ($dist) {
                    $totalPenjualan1 = TransactionHistory::where('id_distributor', $dist['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $dist['id'])->sum('total');
    
                    $dist['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $dist;
                });
                $datas = $distributors->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'superadmin_distributor')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
            }

            $pdf = PDF::loadView('dashboard.export_detail_table', compact(['user', 'type', 'datas']));
            return $pdf->download($user.' '.$type.' - '.date('F Y').'.pdf');
    
        }
        else
        {
            if($type == "stok")
            {
                $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                if(auth()->user()->id_group == 1)
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
                else
                {
                    $datas = Product::join('users', 'users.id', '=', 'products.id_owner')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(stok) as stok')->orderBy('stok', 'desc')->get();
                }
            }
            else if($type == "penjualan")
            {
                if(auth()->user()->id_group == 1)
                {
                    $resellers = User::where('user_position', 'reseller')->orWhere('user_position', 'sales')->orderBy('id_group', 'asc')->get();
                }
                else
                {
                    $resellers = User::where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->orderBy('id_group', 'asc')->get();
                }

                $resellers->map(function ($res) {
                    $totalPenjualan1 = TrackingSalesHistory::where('id_reseller', $res['id'])->sum('total');
                    $totalPenjualan2 = TransactionHistorySell::where('id_owner', $res['id'])->sum('total');
    
                    $res['totalPenjualan'] = $totalPenjualan1 + $totalPenjualan2;
                    return $res;
                });
                $datas = $resellers->sortByDesc(function($d)
                {
                    return $d->totalPenjualan;
                });
            }
            else if($type == "retur")
            {
                if(auth()->user()->id_group == 1)
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->orWhere('user_position', 'sales')->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
                else
                {
                    $datas = User::leftJoin('retur_histories', 'retur_histories.id_owner', '=', 'users.id')->where('user_position', 'reseller')->where('users.id_group', auth()->user()->id_group)->orWhere('user_position', 'sales')->where('users.id_group', auth()->user()->id_group)->groupBy('users.id')->select('users.*')->selectRaw('sum(total) as totalRetur')->orderBy('totalRetur', 'desc')->get();
                }
            }

            $pdf = PDF::loadView('dashboard.export_detail_table', compact(['user', 'type', 'datas']));
            return $pdf->download($user.' '.$type.' - '.date('F Y').'.pdf');
        }
    }
}
